# Good Technologies Taskfiles

A collection of task files for common tasks.

## How to use

To use these Taskfiles, all you need to do is clone this repository to your
local machine, then include the required Task file in you own project's
Taskfile.

```shell
git clone git@gitlab.com:good-technologies-oss/taskfiles.git
```

```yaml
# Your Taskfile.yml
version: '3'

includes:
  docker-compose:
    taskfile: './path/to/good-technologies-oss/taskfiles/docker/DockerComposeTaskfile.yml'
    aliases: [ 'dc' ]
```